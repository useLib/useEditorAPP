Dateiablage bzw. 'Installation'

Browser bzw. JavaScript haben aufgrund früheren bösartigen Mißbrauch starke Restriktionen beim file://-Protokoll. Die Html-APPs können sogar auch lokal unter diesem Protokoll erzeugt werden, dass ist aber etwas umständlich, da die erwarteten bzw. benötigten Dateien von Hand auf das Browser-Fenster geschoben werden müssen.

Empfohlen wird der nachhaltige Betrieb im http://localhost, also das Öffnen der jeweiligen Html-Datei mit dem entsprechenden zughörigen Pfad. Natürlich ist auch der Betrieb im Intranet bzw. einer Cloud möglich. Alle Pfade im Kopf der Html-Vorlage sowie ggf. auch die Pfade in der Datei: useLibConf.js müssen entsprechend angepasst werden.

Vordefiniert ist die folgende Datei- bzw. Pfad-Struktur:

Root ist die jeweilige Html-Vorlage (oder auch mehrere)
<pre>- useLib - designs - useLib   - useLib.css
                                useLibInfoV1.png
                                useLibIconsV1.sw.png
                   - vorlagen - fullskinexample.png
                                dg_lBl.gif
                                dg_lBr.gif
                                dg_lEl.gif
                                etc.
                     standard.css
                     oldstyle.css
                     intoTheBlue.css
                     greenLegend.css
                     etc.
         - wikis   - useEditor.de.txt
                     useLibStart.de.txt
                     wikibegin.de.txt
                     wikiexpert.de.txt
                     wikiadvanced.de.txt
                     etc.
         - sounds  - alarm.mp3
                     error.wav
                     inputHint.wav
                     etc.
           useLibConf.min.js
           useLib.min.js
           uLwiki.min.js
           uLsecurity.min.js
           etc.
</pre>