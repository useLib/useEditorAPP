# useEditorAPP

Achtung: Alle HTML-Dateien sind alleinstehend ausführbare Anwendungen, einfach runterladen und im Browser öffnen!

Der //useEditor// ermöglicht es ++auch Computerlaien++ optimal an ihre Arbeitsaufgaben angepasste ++Inhaltstexte++ und ++Formulare++ inkl. Beschreibung zu ++erzeugen++.
Natürlich kann auch ++jeder beliebige, gewohnte Editor++ genutzt werden kann, um die verarbeiteten sog. Wiki-Dateien im Text-Format zu erzeugen. Die Formatierung der Inhalte von Wiki-Dateien lehnt sich an dem erprobt einfachen ++Standard von Wikipedia++ an, der sich ebenfalls an Computerlaien richtet. Diese Version des useEditors enthält (fast) die ++gesamte Dokumentation++ des client-seitigen Teils ++der useLib++.

Der //useEditor// kann und soll zum einen die Erzeugung von Dokumentationen, Formularen, ganzen Web-Sites und HTML-Apps in Form von Wiki-Dateien erleichtern und direkte Rückmeldungen bei Änderungen geben und deren Überarbeitung auch in Versionierungen unterstützen. Dadurch kann der //useEditor// als zentraler Baustein der ++digitalen Souveränität++ und der ++Selbstermächtigung++ der Benutzenden dienen. Programmierende können auch eine ""einfache"", funktionierende Standardversion erzeugen, deren Auswahlfelder und Reihenfolgen von Eingabefeldern dann seitens der User an die jeweilige Arbeitsaufgabe angepasst werden.  

Benutzer können ++spezifische Oberflächenentwürfe++ auch mit komplexen Interaktionen nicht nur direkt ++ausprobieren++, sondern eigene Varianten davon erzeugen. Insbesondere aber können Benutzende ihr Know-How direkt mittels der ++Beschreibung von Praxisbeispielen++ einbringen und so für neue Mitarbeitende ++Einarbeitungsprozesse erheblich verbessern++ helfen. Hierzu bindet der //useEditor// alle auch einzeln verwendbaren JavaScripte der useLib ein und verbindet diese zu einem ++umfassenden Metakonzept++. Er kann mit einem überaus breiten Anwendungsspektrum als ++universelles Customizing-Tool++ eingesetzt werden. Doch zunächst zu den Grundfunktionen:

* Benutzer können per E-Mail versendete Formular- und Textentwürfe ++direkt ausprobieren++.
* Sie können ++eigene Verbesserungsvorschläge++ durch Anpassungen der Vorlagen zurück senden. Ein im Vorschaufenster markierter Text wird ++gleichzeitig im Editor zum Bearbeiten selektiert++.
* Benutzergruppen können in kleinen Workshops einzelne Felder Erläuterungen und Beispielen ++aus der täglichen Praxis++ ergänzen.
* Im Rahmen der ++Internationalisierung++ können von Benutzern Übersetzungen optimiert werden.
* Viele weitere Anwendungen auch zum ++lokalen Einsatz++ sind einfach umzusetzen. Das Spektrum reicht von Hilfesystemen bis hin zu kleinen Anwendungen zum hoch komplexen Dokumentenmanagement (DMS).

