{{title=netzgestützte Eingaben}}{{version=1.0}}{{author=Dr.D.Fischer@use-optimierung.de}}

= Ein Beispiel zur useLib-Funktionalität bei Aufgaben mit Netzzugriff

<< Es werden je nach Feld einzelne Datensätze hinzu geladen, z. B. die tagesaktuellen Wechselkurse von der EZB.

Solange das Fenster bzw. der Reiter nicht geschlossen wird, bleiben diese Daten zugriffbereit. D. h. auch ggf. größere Datensätze werden nur einmal geladen!>>

<<<<Es gibt einige Standarddatensätze der useLib, wie z. B. einen mit diversen Daten zu den Ländern der Erde wie IBAN-Struktur, Telefonvorwahl, Währung oder auch einen Datensatz mit allen ISO-Einheiten und diversen Umrechnungen. Hinzu kommen länderspezifische, die beispielsweise alle Städte mit Postleitzahlen Deutschlands enthalten. Alle möglichen Datensätze wie Kalenderdaten mit Terminen können zu geladen und Benutzern als Eingabe bzw. Auswahlunterstützung zur Verfügung gestellt werden.>>>>

((form;;550 @@ name="testForm" @@

= Datumseingabe mit Kalender
<<Wird ein Kalender mit zugeladen können blockierte Termine zu direkten Rückmeldungen bei der Eingabe führen. Dann ist es meist notwendig genauer im Kalender nach zu schauen, was da für ein Termin liegt. Angezeigt wird ein Kurztext zum Termin, wenn man mit der Maus drauf zeigt      
&&#ff0000(Achtung: Terminfindungsschwierigkeiten;-)&&.>>
<<<<Der hier eingeblendete Kalender erzeugt einige Schwierigkeiten, einen Termin zu finden. Im Projekt-Kalender ist auch die längste Peride auf 5 Tage begrenzt.>>>>

==218 §§sd§§ Startdatum @@ id="date_start" @@
:date @@ name="date_start" data-ul="https://www.use-optimierung.de/useLib/projectCalendar.json.css" @@
==80 Dauer
:daysperiod date_start
==185 §§ed§§ Enddatum
:enddate date_start

= Weitere hilfreiche Zusatzdaten zur Erleichterung der Eingabe
<<Als Standard integriert sind auch Maßeinheiten. Hier gibt es leicht mal Engpässe im Rahmen von internationalen Bestellungen oder Anfragen.>>

== §§th§§ Temperatur mit Umrechnung
:unit @@ value="15.20" data-ul="0F" @@
== §§is§§ Alle ISO-Einheiten zur Auswahl
:isounits @@ value="5387.20" data-ul="mm" @@

== Telefonnummer mit internationaler Vorwahl
<<Nicht immer hat man die Vorwahlnummern parat, die useLib macht sie auswählbar.>>
:fon

== §§wh§§ Preiseingabe mit Währungsumrechnung
<< Die ++tagesgenauen Wechselkurse++ kommen von der Europäischen Zentralbank. Sie werden erst bei Wechselkuranwahl durch Klick: ++Weltsymbol++ geladen.>>
<<<<Als Beispiel wird hier als Eingabebasis der US-Dollar gewählt, Standard ist natürlich EUR. Man kann natürlich auch bei Klick:Wechselsymbol auf diese ++Basis wechseln++. Es wird immer der Wert auf der vordefinierten Basis ans System übermittelt.>>>>
:currency @@ value="5387.20" @@

== §§st§§ Ortsangabe
<< Für Deutschland sind Postleitzahlen und Städtenamen hinterlegt und können auch mittels Pfeiltasten gewählt werden. (Die Datensätze weiterer Länder könnten ebenfalls hinterlegt werden.)>>
:towns

==Eine ganze standardisierte Lieferadresse
<<Die Lieferadresse weicht nur leicht vom ebenfalls vordefinierten Standard: //Anschrift// ab und weist z. B. auf die Felder einer Packstation hin.>>
:Lieferadresse

==50% Urlaubsland
<<Die vollständige Eingabe des Landesnamens in Kombination mit einer Auswahlmöglichkeit.>>
<<<<Dies spiel insbesondere dann eine Rolle, wenn die Länderanzahl nach ihrem Wirtschaftlichen Status beschränkt werden soll. Hierzu kann eine //data//-Anweisung  mit einer einfachen Zahl ergänzt werden:
:15 1 :: Industrienationen (Industrialized countries)
: 2 :: BRICS-Staaten (BRICS countries)
: 3 :: Schwellenländer (emerging nations)
: 4 :: Entwicklungsländer (developing regions)>>>>
:country 2

= IBAN mit Bankverbindung
<<Das Hauptproblem der IBAN-Eingabe liegt für Benutzer in der Länge der einzugebenden Nummer bzw. Kennung, der immer wieder unterschiedlichen Abstandsstrukturierung dieser Nummern z. B. in Ausdrucken sowie der je nach Land unterschiedlichen Aufbau der IBAN. Sowohl sind neben Ziffern ggf. auch Buchstaben erlaubt, als auch die Stellenanzahlen können unterschiedlich sein. Die useLib prüft das IBAN-Format landesspezifisch: Beispielsweise für Spenden an Greenpeace: DE49 43060967 0000033401. Auch Reinkopieren, selbst mit fehlenden führenden Nullen bei der Kontonummer.>>
<<<< Für die Ermittlung der BIC und des Kreditinstitutes kann relativ einfach eine Abfrage an den [[extern https:/\/www.bundesbank.de/SiteGlobals/Forms/Suche_BLZ/Einfache_BlzSuche_Formular.html SCL-Service der Deutschen Bank]] implementiert werden. Dazu muss aber jeweils ein Nutzungs-Account frei gegeben werden. Alternativ kann man die heruntergeladene und aufbereitete [[extern https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen Liste der deutschen Bankleitzahlen]]  verwenden.>>>>

:bankverbindung

))