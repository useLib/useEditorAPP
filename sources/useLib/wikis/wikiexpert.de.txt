﻿{{title=Wiki für Experten}}{{version=0.95}}{{author=Dr.D.Fischer@use-optimierung.de}}

= Wiki für Experten

Die nach den im [[wikiadvanced.de.txt Wiki für Fortgeschrittene]] beschrieben Regeln erzeugten Wiki-Texte können von ++Experten++ mit möglicherweise notwendigen oder ergänzenden Zusatzinformationen wie applikationsinterne Namen der Formularfelder nachträglich versehen werden. Hier werden diese ++ergänzende Möglichkeiten++ beschrieben, die teilweise bis in den Bereich der Programmierung hineinreichen.
Mit etwas ++Übung++ können Wiki-Experten bei Bedarf ++Zusatzangaben bzw. Steuerungsinfos++ zu den Grundelementen zufügen, um deren Ansicht bzw. Formatierung genauer fest zu legen oder ++Javascript-Aufrufe++ etc. tätigen.

== Gliederungszeichen in Tabellen

Natürlich gibt es auch die Möglichkeit Tabellen zu erzeugen. ++Tabellen++ sind nur einfach zu handhaben, wenn relativ kurze, einzelne Werte die auch in eine Zeile passen.

* jede Tabellenzeile wird mit einem §§+§§-Zeichen am ++Zeilenanfang eingeleitet++.
* ++Zellen++ werden am einfachsten durch zwei §§:\:§§-Zeichen von einander ++getrennt++.
* Um Zeilen direkt ++fortzusetzen++, müssen die beiden Trennzeichen direkt dem führenden §§+§§-Zeichen folgen.

Um die Formatierung der Tabelle selbst, aber auch die ihrer Spalten zu kontrollieren, werden solche Angaben mittels einer ++Formatzeile am Tabellenanfang++ festgelegt.

* Bei einer Formatzeile folgt ein §§#§§-Zeichen ++direkt auf++ das §§+§§-Zeichen.
* Dahinter können die normalen ++Zusatzangaben++ bzw. Parameter zu Wiki-Elementen genau wie  bei Blöcken benutzt werden.
* Zusätzlich können ++Breite++ und ++Ausrichtung++ von Tabellenspalten festgelegt werden, indem diese wiederum durch zwei §§:\:§§-Zeichen von einander getrennt die Werte genau wie bei den normalen Zusatzangaben angegeben werden.

Schwieriger wird es, wenn Tabellenzeilen und -spalten zu einer Zelle zusammengefasst werden sollen.

* Ein oder mehrere §§-§§-Zeichen ++verbinden horizontal aufeinander++ folgende Zellen einer Tabelle zu einer Zelle.
* Ein oder mehrere §§*§§-Zeichen ++verbinden vertikal übereinander++ stehende Zellen einer Tabelle zu einer Zelle.
* Diese Angaben können für einzelne Zellen auch kombiniert werden.

<<<<
++Am schwierigsten++ zu beherrschen sind Tabellen, bei denen der Inhalt einzelner Zellen selber wiederrum ein ++mehrzeilig aufgebauter Wiki-Text++ sein soll. Dann muss dieser Inhalt als Block gekennzeichnet sein und die Fortsetzung der jeweiligen Zeile mittels der zwei §§:\:§§-Zeichen genau zu den anderen Tabllenzeilen passen. Dies ist nur für sehr erfahrene Wiki-Nutzer zu empfehlen. So ist es aber z. B. möglich, wiki-formatierte Textpassagen tabellarisch an zu ordnen (siehe die tabellarische Anordnung des folgenden Beispiels mit zwei Blöcken: ein pre-formatierter und eine Wiki-Tabelle).
>>>>

Eine relativ einfache Tabelle mit Zusammenfassung einzelner Spalten und Zeilen zeigt das folgende
Beispiel.

+#400 ::60%;;right::40%;;center
+ am Zeilenanfang :: führt zu
+((pre
+#right;;100% :: 25% :: 25%;;center :: 25% :: 25%

+  s1 ::  s2 :: s3 :: s4
+ 234 :: &&red++// 319 //++&& :: 17 :: 34
+  15 ::-*   56    :: 671
+  12              :: 418
))
+::
((
+#right :: 25% :: 25%;;center :: 25% :: 25%

+  s1 ::  s2 :: s3 :: s4
+ 234 :: &&red++// 319 //++&& :: 17 :: 34
+  15 ::-*   56    :: 671
+  12              :: 418
))

== Ergänzungen zu Verweisen und Bildern

++Verweise und Bilder++ können mit ++zusätzlichen HTML-Attributen++ versehen werden, wie beispielsweise Javascript-Aufrufen oder zur Identifikation des jeweiligen Verweises bzw. Bilder verwendbaren Informationen. Hierzu werden diese einfach in §§@\@§§-Zeichen eingeschlossen und mit in die in die eckigen Link-Klammern eingefügt, beispielsweise:;;
§§@\@ onclick="useLib.alert( 'Es wurde geklickt!' )" @\@§§
(siehe unten bei technischen Einfügungen).

Eine in diesem Zusammenhang besondere Einfügung ist es, wenn man einen ++Anker++ als Ziel eines internen §§#§§-Links setzen will. Hierzu wird die folgende Angabe verwendet:;;
§§[\[# @\@ name="AnkerName" @\@ ]\]§§

== Standardknöpfe in Formularen

An jedes Formular werden ++automatisch Standardknöpfe++ zur Formularsteuerung angehängt. Die übergeordneten Knöpfe sind nicht immer erwünscht und haben auch unterschiedlichen Charakter. Daher sind unterschiedliche Formulartypen vordefiniert.

Die Standardknöpfe eines ++normalen Formulars++ sind:
* ++leeren++ (clear) leert alle Eingabefelder und wählt alle Auswahlwerte im Formular ab.
* ++rücksetzen++ (reset) setzt Eingabefelder und Auswahlwerte auf den Ausgangszustand zurück (dies kann ggf. auch dem leeren Zustand entsprechen).
* ++senden++ (submit) prüft die Daten und schickt diese zur Verarbeitung über das Netzwerk an den verarbeitenden Computer (server engl.).

Ein ++eingeblendeter Dialog++ hat ähnliche Standardknöpfe mit leicht abgewandelter Funktion:
* ++abbrechen++ (cancel) schließt den Dialog ohne Veränderung irgendwelcher Daten.
* ++rücksetzen++ (reset) auf den Ausgangszustand s. o.
* ++übernehmen++ (overtake) der Daten des Unterformulars ins aufrufende Formular bzw. Tabelle.

Eine ++eigene Beschriftung++ anstelle von ++senden++ wird erzeugt, indem nach dem Schlüsselwort ein Text folgt, also beispielsweise //(\(form Eigener Knopftext @\@ ... @\@//. 

== Gliederungszeichen in Formularen

=== Gruppierung Zwischenüberschriften und Labeln

Auswahlfelder aus Radio-Knöpfen oder Häckchen bei einer ++Zwischenüberschrift++ werden grundsätzlich wie ein Fließtext hintereinander dargestellt. Wird eine eine ++Breite++ geschickt gewählt, so werden die Elemente spaltenweise übereinander angeordnet.

Dabei kann es vorkommen, dass man z. B. bei Aufzählungen eine Überschrift zwar wiederholen, aber diese ++Wiederholung kennzeichnen++ möchte. Hierzu kann die wiederholte Überschrift mit dem Schlüsselwort //gleich// bzw. //same// versehen und so bei der Darstellung verdeutlicht werden.

Zur zusätzlichen Formatierung von Formularen können //Schlüsselwort-Überschriften// eingesetzt werden, die auf der jeweiligen Ebene folgende Wirkungen haben:

* bei //Umbruch// (linebreak) in der nächsten Zeile weiter,
* ein mittels Breite steuerbarer //Abstand// (gap) zwischen zwei Elementen
* ein //Trenner// (separator) sorgt für den Abbruch einer Ebene.

++Bei Labeln++ können zum Ansprechen des zugehörigen Formularelements mittels Tastatureingaben ++Kürzel++ festgelegt werden. Diese werden einfach zur Benennung zugefügt und genau wie die Tastatureingabe in §§§\§§§-Zeichen eingeschlossen. Es ist sogar möglich, mehrere Kürzel zu definieren, die durch §§;;§§ getrennt werden.

So können auch z. B. Tastaturkombinationen aus früher genutzen Programmen zugefügt werden, damit automatisierte Eingaben hoch geübter Benutzer weiter funktionieren. (Sondertasten: STRG oder CTRL, ALT und TAB ggf. gefolgt von einem '+' mit Buchstaben.  Die Funktion muss im Einzelfall geprüft werden, da Kürzel bereits vom Internet-Programm (browser) belegt sein können). Angezeigt wird immer nur das erste Kürzel!

Zusätzlich können den Zwischenüberschriften und Label noch weitere Informationen mit gegeben werden, die Bedeutungen auf einer übergeordneten Ebene haben und folgendes bedeuten:

:20 §§:§§ :: der Wert dieses Feldes ist in diesem Zusammenhang oder auch Nutzerrolle geheim (restricted). Solche Klassifizierungen ermöglichen es, dem Datenschutz auch bei standardisierten Formularen gerecht zu werden.
:§§+§§ :: diese Information des Formulars soll als Navigationsinhalt dienen.
:§§#§§ :: in einer Tabellenspalte wird diese Information im Kopf nicht als Suchfeld, sondern als Checkliste dargestellt.

Letztere können für die aufgabenangemessene Gestaltung der Darstellung und Interaktion dieser Formularinhalte in tabellarischen Darstellungen eingesetzt werden.

=== Sonderfall bei Checkboxen, Radioknöpfen und Selektionslisten

Diese Formularelementen ++zur Auswahl++ sind ein ++in der Praxis++ sehr häufiger Fall. Dies können lange Listen von Begriffen ggf. mit integrierten Erläuterungen sein. Technisch benötigen diese Listen oft zugeordnete Werte (value) für die programmtechnische Bearbeitung. Standardmäßig - ohne diese Zuordnung - wird der ++Text++ der jeweiligen Auswahl ++als Wert++ genutzt.

Grundsätzlich ist diese Zuordnung mittels des Interfaces zu HTML möglich §§* Radioknopftext @\@ value="wert" @\@§§, dies erscheint aber in vielen Fällen zu aufwendig und ist zudem schlecht lesbar. Eine kleine Abkürzung wurde daher eingebaut: §§* $wert Radioknopftext§§. mittels des Auftakt §§$§§ vor dem Wert lassen sich einfach viele Werte direkt zuordnen.

Die zugehörigen Listen lassen sich zwar mehr oder weniger vollständig formulieren, aber es gibt ++hin und wieder Ausnahmen++, die nicht mit abgebildet sind. Dem trägt die useLib ebenfalls Rechnung.

Wird das Schlüsselwort: ++//Eigene Eingabe//++ oder //own input// (ownInput) als Benennung eines Auswahlelementes angegeben, so erscheint bei Verwendung der useLib bei Anwahl eine Dialogbox zur Eingabe eines dann an dieser Stelle eingeblendeten und ans System weiter gegebenen Wertes.

=== [[# @@ name="weitereFormate" @@ ]] Weitere Formularelemente zur Eingabe oder auch formatierte Eingabefelder

Es wurden in den Grundlagen bereits einige vordefinierte Eingabefelder vorgestellt, aufgrund ++mangelnder Standards++ gibt es ++viele Varianten++, die man in  bereits eingesetzten Anwendungen finden kann. Ergänzt werden diese durch weitere besonders ausgezeichnete Felder.

* zusätzliche Datums- und Zeiteingaben auch zur Abbildung von Altsystemen
*: kurzDatum :: (smallDate) wie Datum aber mit zweistelliger Jahreszahl
*: kurzEndDatum :: (smallEnddate) wie Enddatum mit zweistelliger Jahreszahl
*: kurzzeit :: (smalltime) wie Zeiteingabe, aber zusätzlich mit Sekunden
*: endkurzzeit :: (endsmalltime) wie Zeiteingabe, aber zusätzlich mit Sekunden
*: kurzDatumZeit :: (smallDateTime) wie datumZeit mit zweistelliger Jahreszahl
*: endkurzDatumZeit :: (endsmalldatetime) wie kurzddatumZeit mit Pflichtparameter: Benennung des Anfangsdatums durch §§;;§§ abgetrennt

* zusätzliche Zeiträume auch zur Abbildung von Altsystemen
*:100 datumtagedauer :: (datedaysperiod) Eingabezeile für Datum mit Dauer in Tagen
*: tageDauer ::(daysPeriod)  Eingabe für Zeitraum in Tagen mit Parameter: Label des Anfangsdatums durch §§;;§§ abgetrennt
*: zeitdauer :: (timeperiod) Eingabe für Zeitraum in Stunden, Minuten
*: kurzzeitdauer :: (smalltimeperiod) wie Zeitdauer, aber zusätzlich mit Sekunden
*: zeittagedauer :: (timedaysperiod) wie Tagedauer, aber zusätzlich mit Zeitangabe in Stunden und Minuten

* Durch Pseudo-Eingabefelder ergänzte Felder, die Fehlerminderungen bei den Eingaben bedingen, aber nicht zum BackEnd übermittelt werden:
*: datumstagedauer-pseudo :: (datedaysperiodpseudo) mit zusätzlicher Anzeige der Dauer, für die sinnvolle Kombination mit einem EndDatum.
*: zeitdauerpseudo :: (timeperiodpseudo) analog zu //datumstagedauerpseudo// nur für eine Zeitangabe.
*: wochepseudo :: (weekpseudo) mit zusätzlicher Jahresauswahl

* Standardeingaben zur besonderen Verwendung
*: Passwort :: (password) Passwordeingabe (incl. Beschreibung) mit Passwortqualität 75%
*: Passwortwiederholung :: (passwordrepeat) für den Fall, dass sich die Wiederholung der Passworteingabe nicht durch den Prozess vermeiden lässt...
*: Farbe :: (color) Auswahldialog für Farbe etwas verbessert gegenüber dem Standard.
*: suche (search) :: dieser HTML-Standard soll zumindest mit formatierbar sein. Die technische Umsetzung muss aber außerhalb der useLib erfolgen.
*: versteckt (hidden) :: dieser HTML-Standard kann auch zum Einsatz kommen, wenn das BackEnd ein einen Formularinhalt eines bestimmten Namens erwartet, dessen Inhalte aber bei der abgebildeten Arbeitsaufgabe nicht verändert werden sollen. D. h. es wird ggf. nur der angegebene Platz besetzt, das Element selber wird aber nicht angezeigt.
*: calc :: dieses Feldtyp definiert zusammen mit einem oncalc-Attribut ein dynamisch berechneten Feldinhalt. Das oncalc-Attribut muss einen Funktionsnamen enthalten (ohne wird der Feldtyp ignoriert.). Diese Funktion wird aufgerufen, wenn innerhalb des aktuellen umgebenden Fieldsets ein Wert geändert wird. An diese Funktion übergeben werden das Element für den Berechnungsinhalt und das aktuell geänderte (elementForCalcedResult, changedElement). Die Funktion muss das Berechnungsergebnis selber eintragen.

Für die Eingabe des ++Passworts++ wird neben der integrierten algorithmischen Qualitätsprüfung mit direkter Rückmeldung auch ein spezielles Verfahren zum Einsatz eines grafischen Passworts angeboten. Die Passworteingabe erfolgt ebenfalls formatiert in 5-Gruppen, so dass sich die Fehlerrate insbesondere bei der Eingabe sehr langer Passworte reduziert.

Bei durch ein ++Welt-Symbol++ gekennzeichnete Formularelemente, also //fon, towns, currency, iban// und //country//, kann mittels des HTML-Interfaces: @\@ data-ul="2" @\@ die Auswahl an auswählbaren Ländern eingeschränkt werden. Größere Angaben enthalten die kleineren.

:15 1 :: Industrienationen (Industrialized countries)
: 2 :: BRICS-Staaten (BRICS countries)
: 3 :: Schwellenländer (emerging nations)
: 4 :: Entwicklungsländer (developing regions)

== Technische Einfügungen

Es gibt zwei Typen von technische Einfügungen, sie sollten nur von Personen eingesetzt werden, denen die technischen Hintergründe bekannt sind.

* In §§@\@§§ eingeschlossen bilden diese das ++Interface zu HTML++. Alle HTML-Attribute können hier an Formularelemente übergeben und so auch dynamisiert werden.

* Durch §§{\{ }\}§§ werden ++Wiki-Variablen++ gekennzeichnet. Neben einfachen Variablen bilden diese auch progammtechnische Funktionen ab.

=== HTML-Interface

Besonders ++häufig++ wird man ++folgende Attribute++ verwenden:

:50 value :: bei Auswahlelementen wird ansonsten die ++Benennung als Wert++ eingetragen, wo bei Zeichen wie z. B. Umlaute zur Standardisierung entfernt werden. Bei Eingabefeldern ist die Vorgabe leer. (Alle Auswahlen und Eingaben können durch die Schnittstellen der useLib durch das BackEnd gesetzt werden).

: name :: ist ein HTML-Standard, der zum einen ++bei Verweisen++ genutzt wird, um an die mit diesem Namen markierte Stellen zu springen (Anker bzw. anchor). Bei Formularelementen, ist dies der Name mit dem ++im BackEnd++ die Formularinhalte ++zugeordnet++ werden.;;
Wird der Name //ignorieren// oder auch //ignore// gewählt, so wird dieses Feld __nicht__ an den Server übermittelt, so lassne sich in die Seite ggf. auch Hilfsfelder, z. B. für Währungsumrechungen bei Prüfaufgaben integrieren.

: onclick :: weißt einer Maus- oder Touch-Interaktion eine dann ++auszuführende Javascript-Funktion++ zu. Diese können ggf. auch lokal definiert werden.

: id :: um von bestimmten von der useLib ++unabhängigen Scripten++ angesprochen werden zu können.

: pattern :: beispielsweise um eigene ++formatierte Eingaben++ zu kreieren.

: data-ul :: zur Übergabe von speziellen useLib-Paramenter, beispielsweise bei Formularen.

=== Wiki-Variablen

Einfache Wiki-Variablen werden als ++Ersetzungstext++ definiert, beispielsweise
§§{\{ersatzWort ein völlig anderes Wort oder auch Satz}\}§§ {{ersatzWort ein völlig anderes Wort}}
führt beim späteren Einfügen von §§{\{ersatzWort}\}§§ zum Ersatz durch //{{ersatzWort}}//.

Weitere ++vordefinierte Variablen++ dienen der Benutzerinformation beispielsweise beim Einsatz in Webseiten oder auch der eindeutigen Identifikation des jeweiligen Wiki-Textes, wie dies beim //useEditor// der Fall ist. Jede Wiki-Datei sollte beispielsweise die Angaben für Titel, Author und Version enthalten. Die vordefinierten Parameternamen sind an HTML angelehnt nur auf englisch definiert. Ihre Verwendung benötigt teilweise tiefere HTML-Kenntnisse. Prinzipiell kann in einer Wiki-Datei damit alles umgesetzt werden, was auch HTML kann!

* Vordefinierte Standards auch für HTML-Metatags

*:70 title :: Kurztitel als Verweistext für Navigation mit dem ggf. auch auf anderen Seiten bzw. Formularen der jeweilige Text erkannt werden soll.

*: author :: am besten eine E-Mailadresse mit Namen, damit die Zuordnung eindeutig und eine Identifikation ebenfalls möglich ist.

*: coauthors :: eine durch Komma getrennte Liste von Namen im E-Mail-Format der zu erwähnenden Menschen. Leider gibt es keinen HTML-Metatag dafür. Es können auch Pseudo-E-Mail-Adressen verwendet werden.

*: version :: am besten Datum mit Uhrzeit, um auch eine zeitliche Zuordnung vornhmen zu können.

*: description :: Beschreibung des Zwecks des jeweiligen Inhalts. Ggf. mit weiteren Verwendunghinweisen.

*: keywords :: Wichtige Begriffe oder Schlagworte zum Text, was auch das Auffinden durch Suchfunktionen erleichtert. Bei Web-Seiten dient dies auch der SEO-Optimierung.

*: section :: Wird bei der Erzeugung von mehrseitigen Formularen, HTML-Apps bzw. Websites verwendet. Dient als barrierefreie Orientierungshilfe und der Festlegung der Reihenfolge in der Navigation.

*: copyright :: ein Internet-Standard auch für Robots, der helfen kann eine Urheberschaft zu sichern.

*: date :: ein Internet-Standard auch für Robots, der der Verfolgung von Versionierungen dienen soll. ++Wichtig: die Datumangabe muss im Ziffern-Format Jahr-Monat-Tag (YYYY-MM-DD) erfolgen.

*: language :: es ist möglich ebenfalls die Sprache zu kennzeichnen. In einer erzeugten HTML-Datei wird dann diese Sprache eingestellt, so dass beispielsweise //:lang//-CSS-Deklarationen und vordefinierte Texte dieser Sprache verwendet werden.

*: filename :: mit dieser Variablen kann als //Section// auch eine externe Datei im Navigationsbereich eingefühgt werden. Im Falle der Verwendung als CMS kann so der einer Section zu gewiesene Dateiname festgelegt werden. Ansonsten wird der //include//-Name oder der Titel als Dateiname benutzt. Letzterer von nicht erlaubten Zeichen gegreinigt.

* Tiefergehende Funktionalitäten, für die ggf. Netzwerk oder HTML-Kenntnisse erforderlich sind.

*:70 classes :: dient dem ++Anmelden++ von anderen, als den bereits in der useLib integrierten ++Darstellungsklassen++, welche in einem Stylesheet von Designern zu speziellen Zwecken definiert sein können. Klassennamen werden einfach durch ++Leerzeichen getrennt++ aufgezählt.

*: include :: Dies ist die komplexeste Wiki-Variable deren Funktion auch nicht immer gewährleistet sein kann, da Pfade nicht zur ++vorliegenden Installation++ passen oder aber aufgrund von ++Sicherheitseinstellungen++ der Browser nicht zugänglich sind. Dieser Sachverhalt wird beim //useEditor// berücksichtigt. Es kann eine Version erzeugt werden, die alle Includes in einer ++funktionierenden Umgebung++ einbindet und in die erzeugte HTML-Datei integriert. Dabei gilt die Regel, dass die integrierten Dateien relativ zum //useLibPath// liegen müssen, der in useLibCon.js definiert ist. D. h. absolute Pfade, die mit //https:/\/...beginnen, werden in allen erzeugbaren Varianten (versucht) zur Laufzeit hinzuzuladen. Dies kann also bei lokalem Einsatz und fehlendem Internet- bzw. Intranet-Zugriff auch schief gehen.

*: style :: dient dem Einbinden und der Definition von ++CSS-Styles++. Die Style.Definitionen können entweder als Kode hinter dem Schlüsselwort eingefügt werden oder als Pfad auf ein Stylesheet, in dem der Pfad direkt mit einem Gleichheitszeichen analog zum //include// zugewiesen wird. Absolute Pfade werden immer zur Laufzeit hinzugeladen. Wird eine //style//-Deklaration vor der ersten //section//-Deklaration vorgenommen, so gilt diese global in allen  Abschnitten (sections) - definiert innerhalb einer //section// wirkt also nur lokal dort.

*: script :: dient dem Einbinden und ++Ausführen von Javascript++. Das Script kann entweder als Kode hinter dem Schlüsselwort eingefügt werden oder als Pfad auf eine Script-Datei, in dem der Pfad direkt mit einem Gleichheitszeichen analog zum //include// zugewiesen wird. Absolute Pfade werden immer zur Laufzeit hinzugeladen. Wird eine //script//-Deklaration vor der ersten //section//-Deklaration vorgenommen, so gilt diese global in allen Abschnitten (sections) - definiert innerhalb einer //section// wirkt also nur lokal dort.

*: extend :: sehr ähnlich zu //script//. Es ist manchmal sinnvoll oder notwendig, globale Script-Elemente auch innerhalb einer //section// zu definieren. In //extend// eingeschlossene Javascript-Befehle werden daher ebenso global ausgeführt, wie //script//-Deklaration vor der ersten //section//-Deklaration.

*: dynamicSelect :: dient der Definition besonders großer (häufig wiederkehrende) Auswahllisten. Im Prinzip kann das Ergebnis eines Card-Sorting direkt übernommen werden. In Kombination mit dynamischen Listen (siehe wikiEnsemble) bewirkt diese Listendefinition eine Fülle von Möglichkeiten, insbesondere da Bereiche von Checkboxen und Radioknöpfen miteinander kombiniert werden können.

*: wikiEnsemble :: dient der vereinfachten ++Definition einer wikiEnsemble-Struktur++ für wiederkehrenden Meta-Controls wie beispielsweise jenes zur Adresseingabe und für dynamische Listen direkt aus dem Wiki-Text heraus. Dies funktioniert grundsätzlich analog zu //extend//, aber die Methode //useLib.extendWikiEnsemble// für die Bekanntmachung der gewünschten Struktur in der useLib wird quasi aus einer Wiki-Formular-Definition heraus generiert (s.u.).

Zu ++//section//++: Wird //section// ein numerischer Wert, z. B. §§2.3§§, zugewiesen, so wird dieser als Gliederungspunkt interpretiert, der dann in der generierten Navigation enthalten ist. Ist dieser Wert in der //landmarks// Struktur der useLib-Konfiguration definiert, so wird der Inhalt dieser //section// diesem //landmark//-Bereich des vom //useEditor// erzeugten HTMLs zugewiesen.

Zu ++//dynamicSelect//++: Die Definition erfolgt als einfache Texthierarchie:

* Die Überschrift 1.Ordnung §§=§§ definiert den programmtechnischen Namen der Select-Struktur (ohne Mehrsprachigkeit).
* Die der 2.Ordnung §§=§§ definiert eine Gruppenüberschrift
* Mittels §§*§§ wird ein Radioknopf definiert
* Mittels §§#§§ eine Checkbox wobei beide Anwahltypen frei gemischt werden dürfen.

Wichtig ist, dass bis auf die Überschrift 1. Ordnung alle Elemente in einer Zeile mehrsprachigen bzw. technisch durch §§:\:§§ getrennt angegeben werden können bzw. sollten (s. u.). Ein umfangreiches Beispiel findet sich in den [[expertformtest.de.txt Eingaben für Experten]] wie auch für wikiEnsemble.

Zu ++//wikiEnsemble//++: Die Erzeugung der etwas komplexere JSON-Objekt-Struktur für die Methode //useLib.extendWikiEnsemble// ist durchaus aufwendig. Dabei schleichen sich leicht Fehler ein, insbesondere wenn kleine Änderungen ausprobiert werden sollen. Da die Listenelemente aus sich wiederholenden Formularelementen bestehen, ist es nahe liegend, diese analog zu wiki-Formularen zu definieren (analog zu Fieldsets). Die Definition ist etwas strenger und ergänzende Texte sind nicht erlaubt.

Wichtig ist, dass jedes Element der 2.Stufe §§==§§ ein eindeutige Benennung benötigt - insbesondere die zur Auswahl. Am besten ist eine mehrsprachige inkl. technischer Feldbenennung. Eine Mehrsprachigkeit ist so auch programmtechnisch einfach abzubilden(s. u.). Die Analogie zu den Formularen ergibt sich aus dem Folgenden: 

:15 §§=§§:: programmtechnische expand-Benennung (1. Stufe), Feldbenennungen (2. Stufe)
: §§#§§:: für Häkchen und Auswahlfelder aus Häckchen (checkbox) mit Rückgabewert (value) (ohne wird englische Sprachdefinition zurück gegeben),
: §§*§§:: für einfache Schalter und Auswahlfelder aus Radio-Knöpfen (radio) mit Rückgabewert (value) (ohne wird englische Sprachdefinition zurück gegeben),
: §§-§§:: für einfache Auswahllisten (selectlist) inkl. Sprachdefinition (option),
: §§+§§:: für Mehrfachwahllisten (multiselectlist) inkl. Sprachdefinition (option), 
: §§:§§:: für Eingabefelder und Auswahlselektoren, die mittels Benennung festgelegt werden sowie
: §§:\:§§:: Trennung von mehrsprachigen und technischen Benennungen bei Feldbenennungen und Wahllisten [ deutsch :\: englisch :\: programminterner Name :\: ggf. Berechnungstyp ].

Eine ++Besonderheit++ bzw. wertiges Feature liegt in der Möglichkeit der Angabe eines Berechnungstyps zur ++automatischen Berechnung++. Dann wird am Ende dynamischer Listen eine Ergebniszeile eingefügt, die das Berechnungsergebnis für die jeweilige Spalte anzeigt. Folgende Berechnungen sind möglich und werden mit den Schlüsselworten: //count, min, max, sum, average, deviation// ausgelöst. Also automatische Bestimmung von ++Anzahl, Minimum, Maximum, Summe, Durchschnitt, maximale Abweichung vom Durchschnitt++ in der jeweiligen Spalte. Wird keine Angabe gemacht, bleibt die Ergebnisspalte leer.;;
++Achtung:++ Bei mehrzeiligen Ensembles werden/können nur die Felder der letzten Zeile zu einer solchen Berechnung zusammen geführt werden!

Zu ++//mehrsprachigen Benennungen//++ gilt für alle Sprachfestlegungen:

* Sie müssen hintereinander durch §§:\:§§ getrennt angegeben werden.
* Die Reihenfolge ist im Standard als //deutsch :\: englisch :\: programmtechnischer Code// festgelegt. Für dynamische Listen wird noch das Schlüsselwort für die Berechnung angehängt.  
* Sind weniger Angaben gemacht, wird immer die vorhergehende Angabe für die nächste Zuordnung übernommen. Das bedeutet, wird beispielsweise nur eine Angabe gemacht, wird die deutsche Bezeichnung auch in englischen Sprachumgebungen angezeigt und diese Angabe als programmtechnischer Rückgabewert genutzt.
* Bei Häckchen und Radio-Knöpfen und Auswahllisten wird entweder der englische Text oder der Text hinter dem Auftaktzeichen als programmtechischer Rückgabewert (value) genutzt. Das ist anders als bei Eingabefeldern, da dort die Eingabe an den Server bzw. die hinterlegte Datenbank gesendet wird.
* Auswahllisten und Mehrfachwahllisten ist dies der letzte Sprach-Wert hinter dem Auftaktzeichen.

=== Gruppierende bzw. felderzeugende Eingaben

Diese neuartigen Eingabeklassen lösen eine in vielen Anwendungen auftretende Anforderung: die dynamischen Erweiterung einzelner Eingaben zu einer Eingabeliste unbekannter Länge beispielsweise von E-Mail-Adressen. Ein Eingabefeld wird vorgegeben und weitere können dann angefügt werden.

Um dies zu erreichen, muss einfach hinter diesem Feld ein spezielles Eingabefeld zugefügt werden, dass zur entsprechenden Steuerung dient. In diesem Feld selbst können keine Eingaben vorgenommen werden. Ein Klick auf das Feld fügt dann die Erweiterung ein. Wichtig ist, dass dieses dann automatisch verwaltet (zufügen, löschen etc.) wird.

:90 gruppierer :: (grouper) Ein Gruppierer folgt einem beliebigen Eingabefeld, welches dann automatisch gewandelt und verwaltet wird. Wichtig ist, dass der mittels des technisches Interfaces §§@\@§§ definierte HTML-Name als Feld gekennzeichnet ist, also auf ""[]"" endet. 

: selbstgruppierer :: (selfgrouper) Ein Selbstgruppierer geht über einen Gruppierer hinaus. Dieses Interaktionselement wird innerhalb dynamische Listen (s. u.) eingesetzt und erzeugt aus einem Eingabefeld innerhalb des //Ensembles// einer solchen Liste ein //dynamicselect// zur dynamischen Auswahl eines Listenelementes bzw. -zeile. Umfangreiche selbstbezügliche Definitionen und Gruppierungen sind so ermöglicht.

++Zu //selbstgruppierer//++: Dies ist wohl die komplexeste Anwendung des useEditors in Kombination mit den Möglichkeiten, welche die //useLib// bietet. Die Einsatzmöglichkeiten sind vielfältig. Am gebräuchlichsten und leichtesten nachvollziehbar ist dies am Beispiel der Erstellung von E-Mail-Verteilerlisten. Ausgehend von einer beliebig großen Liste von E-Mail-Adressen können Gruppen definiert werden, die bestimmte E-Mail-Adressen enthalten. Allerdings können beim //Selbstgruppierer// auch Gruppen oder Gruppen von Gruppen gebildet und bei Bedarf beispielsweise importiert, vereinzelt oder von Dopplungen befreit werden. Diverse zusätzliche Filter-, Verwaltungs- und Prüffunktionalitäten sind implementiert. 

Die Definition erfolgt analog zu //gruppierer//. Allerdings muss zusätzlich das Feld vor //selbstgruppierer// im //ensemble// ein Eingabeelement mit §§@\@ data="selfgrouper" @\@§§ gekennzeichnet sein. 
Außerdem darf das dem //selbstgruppierer// vorangestellte Eingabefeld nicht beliebig sein, sondern muss als //dynamicselect// definiert werden. Dieses als //singleSelect// dargestellte Feld, wird durch die automatische Verwaltung bei in der Nutzung vorgenommenen Änderungen jeweils aktualisiert. Benutzenden wird bei Anwahl jeweils eine geordnete Listen aller Werte des mittels //data//-Tags gekennzeichneten //selbstgruppierers// angeboten.

=== Dynamische Listen

Eine weitere hochkomplexe Anwendung des //useEditors// ist die Definition dynamischer Listen. Unterschieden werden die in einer Überschrift anzugebenden Schüsselworte:;;
++dynamiclist++ oder ++dynamicarray++

Der Auftakt //dynamic// bedeutet, dass an letztlich beliebig langen Listen mögliche Manipulationen auch an mehreren Listenelementen gleichzeitig per checkbox angewählt werden können. Die Endung //list// bedeutet, dass jedes Feldelement einen eigenen Namen haben muss. Dieser Name darf aufgrund von allgemeinen IT-Vorgaben für solche Strukturen leider nur aus Zahlen, Klein-Groß-Buchstaben ohne Umlaute etc. sowie Binde- und Unterstrich bestehen (//a-zA-Z0-9_-//). Damit auch eine leere //dynamiclist// für User verständlich dagestellt werden kann, kann hinter die Definition des Ensembles noch eine Bezeichnung für die Vorlage zugefügt werden (//:myEnsemble LeerBezeichnung//)

Ein //array// hingegen verwaltet alle Elemente unter einem Namen als Feld, weshalb dieser Name mit //[]// enden muss. Jede dynamische Liste muss mit einem Namen definiert werden, der durch einen Punkt abgetrennt mit dem in der Konfiguration definierten //dynListKey// beginnt, also beispielsweise ""myEnsemble.feldname[]"".

Dynamische Listen können als wie in diversen real vorkommenden Datenstrukturen erforderlich verschachtelt werden. Zusätzlich ist es möglich eine ++Zeile für Berechnungsergebnisse++ unter den dynamischen Listen einzufügen. Für jede Spalte können analog zu einer Tabellenkalkulation folgende Größen durch Angabe des englischen Schlüsselwortes berechnet werden: count (Anzahl), min (Minimalwert), max (Maximawert), sum (Summe), average (Mittelwert), deviation (Standardabweichung).

Um mit eigenen JavaScript-Funktionen auf Benutzeraktionen zu reagieren, kann man die Events //ondelete//, //onresize// und //onexport// mit einer Funktion belegen. An diese wird immer der wikiEnsembleName, gelöschte bzw. aktuelle zugehörige JSON-Struktur, ggf. Name der kopierten Elements, ggf. neu vergebener Name der Kopie, z. B. myFunction = function(exType, js, sourceName, copyName){...}, mittels §§@\@ name="myEnsemble.feldname[]" onresize="myFunction()" @\@§§.

Für jede dynamische Liste muss eine //wikiEnsemble-Struktur// definiert werden, die dann als Schlüsselwort für das Formularelement der jeweiligen dynamischen Liste angegeben wird. Der useEditor unterstützt dies innerhalb der Wiki-Variablen durch das vordefinierte Interface: wikiEnsemble.

((beispiel
	{\{wikiEnsemble 
 
	= Bezeichner auf deutsch :: Bezeichner auf englisch :: myEnsemble;;
	==20% erstes Feld;;
	:feldtype;;
	...

	}\}

...und irgendwo im Formularbereich...

	Beispiel für dynamiclist @\@ name="myEnsemble.feldname" onresize="myFunction()" @\@;;
	:myEnsemble LeerBezeichnung

	Beispiel für dynamicarray @\@ name="myEnsemble.feldname[]" onexport="exFunction()" @\@;;
	:myEnsemble
))

Möchte man die Listen verschachteln, ist es wichtig darauf zu achten, dass die nächste Ebene mit einem §§=§§-Zeichen mehr eingeleitet wird, da eine Expandstruktur immer automatisch eine zusätzliche Ebene einfügt. Dann zu definierende Namen wiederholen die vorhergehenden Ebenen jeweils durch Punkte getrennt, also beispielsweise ""uLdyn.feldname[].uLdyn.listenelement"".

Die Zeile zum Auslösen von Listen-Manipulationen wird automatisch eingefügt. Für die zusätzliche ++Zeile für Berechnungsergebnisse++ muss dem //langDef// der wikiEnsemble-Struktur ein //calc//-Attribut zugefügt werden, welches genau wie die Label-Bezeichnungen struktiert ist, aber anstelle der Labelnamen die obigen Schlüsselworte oder eben kein Wort einträgt.

Am besten erschließt sich die Definition der Dynamischen Listen an den Quellen der Beispiele. Für eine angemessene Verwendung ist auch eine angepasste Software notwendig, was die Verwendung innerhalb bestehender Anwendungen erschwert. Die useLib erzeugt aus den Daten sehr kompakte, sog. JSON-Strukturen, mit denen solche Listen auch automatisch dynamisch gefüllt werden können. Die Bandbreite der möglichen Anwendungen ist extrem groß und öffnet die Interaktionsoptimierung der useLib hin zur Aufgaben- und Prozessoptimierung.

Zusätzlich können diese Listen noch zusätzlich variiert werden, in dem Zusatzangaben gemacht werden:

:55 noaction :: die Aktionszeile und die Auswahlboxen werden nicht angeboten.
: nofilter :: die Filterungszeile wird auch bei längeren Listen nicht angeboten.
: delete :: auch wenn noaction gesetzt ist, wird ""//löschen//"" angeboten (wobei ""//hochschieben//"" dadurch ersetzt wird.). 
: add :: auch wenn //noaction// gesetzt ist, wird ""//duplizieren//"" angeboten.
: top :: ein dupliziertes Feld wird nicht unten, sondern oben zugefügt. 

== Einlesen und Senden von Formulardaten

Für das Einlesen und Senden von Formulardaten stellt die useLib neben den ++üblichen Übertragungsvarianten++ diverse weitere ++innovative Techniken++ bzw. Schnittstellen zur Verfügung, die auch im Wiki definiert werden können. 

Die üblichen Übertragungsvarianten benötigen einen Zugriffspfad (action) für die Verarbeitung sowie ggf. eine Übertragungsmethode. Der Zugriffspfad wird einfach analog zu Link und Bildern direkt hinter der Block-Definition angegeben. Ohne eine Angabe wird dann die Methode://GET// benutzt. Ist stattdessen die andere Standard-Methode gewünscht, ergänzt man einfach §§@\@ method="POST" @\@§§. Dieser Standard wird durch die useLib stark erweitert, um zum einen die kompaktere Übertragung mit den Zusatzinformationen durch die useLib beispielweise den //Tasks// zu ermöglichen. Zum anderen soll die Übertragung auch payload-verschlüsselt und aus lokal gespeicherten HTML-Dateien heraus sowie über Domain-Grenzen hinweg ermöglicht werden.

Bei allen diesen innovativen Techniken werden die Daten grundsätzlich als JSON-Struktur verarbeitet und übertragen. Die Datenstrukturen können grundsätzlich beliebig komplex sein. Grenzen werden lediglich durch die Speicherverwaltung der Browser gesetzt. Notwendige Strukturen dynamische Formularinhalte werden zur Laufzeit generiert. Sie müssen mit einer zusätzlichen Kennung zur Identifikation der Technik versehen werden. Sie werden als technische Einfügung mittels des HTML-Interfaces beispielsweise @\@ data-ul="AJAXSHA" @\@ eingetragen. Die useLib ist auch für lokale oder nur zeitweise mit dem Internet verbundene Anwendungen gestaltet. Deshalb sind auch Verbindungen über Domain-Grenzen (CORS) hinweg mit umgesetzt. Natürlich müssen dazu im //Backend// die entsprechenden Scripte zur Decodierung und Weiterverarbeitung aktiv sein. Die useLib liefert hier zu entsprechende APIs. Zusammengesetzt sind sie aus der Kombination von Übertragungstechnik und Verschlüsselung.

Folgende Übertragungstechniken werden unterschieden:

:55 AJAX :: Der Datenaustausch erfolgt mittels des dafür in jedem Browser vorgesehenen Standardverfahrens (XMLHttpRequest), ohne die aktive Seite neu zu laden. Die Daten werden per GET abgefragt und per POST an den Ziel-Server übertragen. Dies funktioniert nicht über Domain-Grenzen hinweg.

: JSONCSS :: Diese Kennung für JSONCSS bewirkt eine GET-Übertragung auch über Domain-Grenzen hinweg. Mit dieser lassen sich nicht nur interaktionsunterstützende Daten laden, sondern auch welche z. b. zu Cloud-Services übertragen. ++JSONCSS++ ist ein extra entwickeltes Verfahren, das im Gegensatz zu JSONP einen erheblich sichereren Übermittungsweg per Stylesheets verwendet.

: CORS :: Da der Url-Pfad des JSONCSS in seiner Länge beschränkt ist, müssen größere Datenmengen beispielsweise aus größeren Formularen mittels der POST-Methode übertragen werden. Dies wird mittels der in einem IFRAME zwischen geschalteten useLibCorsAPI.html vom Domain-Server umgesetzt. Dies funktioniert analog zu den Progressive Web App (PWA), aber auch mit älteren Browser-Versionen. Mittels eines Javascripts werden die bereits payload-verschlüsselten Daten per Ajax ausgetauscht.

: ANALYSE :: Diese Übertragungsmethode ist eher experimentell. Sie ist ebenfalls mittels der useLibCorsAPI.html umgesezt. Bei dieser Methode wird eine angesprochene HTML-Datei im Hintergrund für Benutzende unsichtbar geladen. Dann werden die in den HTML-Strukturen enthaltenen Formularinhalte analysiert und der aufrufenden HTML-Anwendung als JSON-Struktur zur Verfügung gestellt. Umgekehrt werden diese ggf. bearbeiteten bzw. ergänzten Daten auch wieder eingetragen und dann rückübermittelt, als wenn diese Seite direkt aufgerufen worden wäre.

Ein zweiter ++SHA-Kennungsteil++ kennzeichnet die ++Verschlüsselung++. Diese wird insbesondere für lokale Anwendungen mit besonders hohen sicherheitstechnischen Anforderungen bereit gestellt. Neben dieser üblichen Transportverschlüsselung mit //https:/\/// bietet die useLib zusätzlich die sehr viel höherwertige Inhaltsdatenverschlüsselung nach dem SHA3-Standard, dem derzeit höchst möglichen Standard. Soll also diese in uLsecurity implementierte Funktionalität aktiviert werden, so wird einfach ++//SHA//++ an die Kennung angehängt.

Für payload-verschlüsselte Zugriffe steht die useLibSecureAPI.php zur Verfügung. Für unverschüsselte Zugriffe ist die useLibJsonAPI.php implementiert. Beide leiten die ggf. entschlüsselten Daten als JSON-Struktur an das im //action//-Tag angegebene Script weiter. Je nachdem, ob die Daten verschlüsselt sind oder nicht, leitet auch die useLibCorsAPI.html die Daten an diese beiden APIs weiter. Mittels dieser API lassen sich ggf. auch E-Mails über die per JavaScript versenden. 